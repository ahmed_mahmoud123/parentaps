var express=require('express');
var app=express();
app.use(express.static('dist'));

app.use(function (req, res, next) {
    //res.header('transfer-encoding', 'chunked');
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Headers', 'x-access-token,Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});

app.get('/*',(req,resp)=>{
  resp.sendFile(__dirname+"/dist/index.html");
});



app.listen(process.env.PORT || 4000);
