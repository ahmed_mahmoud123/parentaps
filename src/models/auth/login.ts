export class LoginModel{

  public config:any;

  constructor(){
    this.config = {
      props:{
        action:"api/login",
        title:"Login Form",
        success_message:"Correct login"
      },
      elements:[
        {type:"textfield",htmlType:"email",value:"",label:"Email",name:"email",required:true,validators:["required","email"]},
        {type:"textfield",htmlType:"password",value:"",label:"Password",name:"password",required:true,validators:["required"]},
        {type:"button",value:"Login",classes:["btn-dark"]}

      ]
    }
  }
}
