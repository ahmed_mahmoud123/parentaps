export class UserProfileModel{

  private elements:Array<any>=[
    {type:"textfield",htmlType:"text",value:"",label:"User name",name:"name",required:true,validators:["required"]},
    {type:"textfield",htmlType:"text",value:"",label:"User job",name:"job",required:true,validators:["required"]},
    {type:"button",value:"Save",classes:["btn-dark"]}

  ]

  constructor(){}

  generateAddConfig(){
    return {
      props:{
        action:"api/users",
        title:"Add User Form",
        success_message:"User added successfully"
      },
      elements:this.elements
    }
  }

  generateEditConfig(){
    let elements = this.elements;
    elements.unshift({type:"textfield",htmlType:"text",value:"",label:"User id",name:"id",required:true,validators:["required"]});
    return {
      props:{
        action:"api/users/",
        title:"Edit User Form",
        edit:true,
        success_message:"User updated successfully"
      },
      elements:elements
    }
  }

  generateViewConfig(user_id){
    return {
      props:{
        title:"User Details",
        byIdUrl:"api/users/"+user_id
      },
      elements:[
        {type:"avatar",name:"avatar"},
        {type:"textfield",htmlType:"text",value:"",label:"First Name",name:"first_name"},
        {type:"textfield",htmlType:"text",value:"",label:"Last Name",name:"last_name"},
      ]
    }
  }
}
