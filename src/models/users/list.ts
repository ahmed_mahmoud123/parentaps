export class ListUserModel{

  public config:any;

  constructor(){
    this.config = {
      dataSource:"api/users?page=",
      detailsUrl:"/dashboard/users/view/",
      deleteUrl:"api/users/",
      delete_message:"User deleted successfully",
      fieldsNames:[
        {name:"avatar",img:true},
        {name:"first_name"},
        {name:"last_name"},
        {
          actions:true,
          btns:[
            {
              title:"View",class:"btn-dark",icon:"fa fa-eye",actionType:"view"
            },
            {
              title:"Delete",class:"btn-dark",icon:"fa fa-minus-circle",actionType:"delete"
            }
          ]
        }
      ],
      colsNames:["Avatar","First Name","Last Name","Actions"],
    }
  }
}
