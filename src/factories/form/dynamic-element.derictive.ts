import { ComponentFactoryResolver,OnInit,Input,Directive,ViewContainerRef } from '@angular/core';

import { TextFieldComponent } from './elements/textfield/textfield.component';
import { ButtonComponent } from './elements/button/button.component';
import { AvatarComponent } from './elements/avatar/avatar.component';

const components={
  'textfield' : TextFieldComponent,
  'button':ButtonComponent,
  'avatar':AvatarComponent

}

@Directive({
  selector : "[dynamicField]"
})


export class DynamicField implements OnInit{
  @Input() config;
  @Input() group;
  /* Directive to generate component in runtime for FORM ELEMENTS */
  constructor(private resolver:ComponentFactoryResolver,private container:ViewContainerRef){

  }

  ngOnInit(){
    // Create Instance from certain component at runtime
    const factory = this.resolver.resolveComponentFactory<any>(components[this.config.type]);
    let component = this.container.createComponent(factory);
    // set config and reactive form group as input to that component
    component.instance.config = this.config;
    component.instance.group = this.group ;
  }
}
