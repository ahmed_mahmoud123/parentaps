import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { APIService } from '../../providers/api.service';
import { ErrorsService } from '../../providers/errors.service';

import { SpinnerModule } from 'angular2-spinner/dist';

import { DynamicField } from './dynamic-element.derictive';
import { DynamicFormComponent } from './dynamic-form.component';
import { ButtonComponent } from './elements/button/button.component';
import { HttpModule } from '@angular/http';
import { AvatarComponent } from './elements/avatar/avatar.component';
import { ValidationComponent } from './elements/validation/validation.component';

import { TextFieldComponent } from './elements/textfield/textfield.component';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpModule,
    SpinnerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: false,
    }),
  ],
  declarations: [
    DynamicFormComponent,
    DynamicField,
    TextFieldComponent,
    ButtonComponent,
    AvatarComponent,
    ValidationComponent
  ],
  exports: [
    DynamicFormComponent,
  ],
  entryComponents:[
    TextFieldComponent,
    ButtonComponent,
    AvatarComponent
  ],providers:[APIService,ErrorsService]
})

export class DynamicFormModule { }
