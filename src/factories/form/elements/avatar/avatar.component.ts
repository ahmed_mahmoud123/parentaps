import { Component } from '@angular/core';
import { BasicElement } from '../basic-element.component';

@Component({
  templateUrl:'./avatar.component.html',
  styles:["avatar.component.css"]
})

export class AvatarComponent extends BasicElement{

}
