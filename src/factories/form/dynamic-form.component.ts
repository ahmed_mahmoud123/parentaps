import { Component,Input,Output,OnInit,EventEmitter} from '@angular/core';
import { FormGroup , Validators ,FormBuilder ,ValidatorFn, FormControl , FormArray } from '@angular/forms';
import { APIService } from '../../providers/api.service';
import { ErrorsService } from '../../providers/errors.service';
import { EmailValidator } from '../../validators/emailValidator';
import { ToastrService } from 'ngx-toastr';

const validations = {
  'required' : Validators.required,
  'email' : EmailValidator.isValid,
}

@Component({
  selector : "dynamic-form",
  templateUrl:"./generic-form.component.html",
  styleUrls:['./generic-form.component.css']
})

export class DynamicFormComponent implements OnInit {
  group:any;
  @Input() config : any = []; /* {props:{action:""} ,elements:[{type:"input",name:"first_name"}] } */
  @Output() afterSubmited : any = new EventEmitter(); /*fire when response back from the server */
  @Output() groupCreated : any = new EventEmitter(); /*fire when form group created */
  working:boolean= false;
  /* for 'config' variable structure kindly check models folder */
  /*Dynamic Form Component to build form depend on 'config' variable which has structure like {props:{},elements:[]}  */
  constructor(private toastr: ToastrService,private apiService:APIService,private errorsService:ErrorsService,private formBuilder:FormBuilder){

  }

  ngOnInit(){
    this.createGroup();
    if(this.config.props.byIdUrl){
      // populate form when update
      this.populateModel();
    }
  }

  /* Build Form group using form elements form 'config' variable */
  private createGroup():void {
    this.group = this.formBuilder.group({});
    this.config.elements.forEach(element=>{
      // get elements from 'config' variable and add it to form group and each element may has validators
      if(element.name)
        this.group.addControl(element.name,this.formBuilder.control(element.value,Validators.compose(this.buildValidatorsArray(element.validators))));
    });
    this.groupCreated.emit({group:this.group,self:this});
  }

  private populateModel():void {
    this.working = true;
    // call web service to get data to populate form
    this.apiService.get(this.config.props.byIdUrl).then(model=>{
      this.working = false;
      if(model){
        for(let property in this.group.controls){
          this.group.controls[property].setValue(model.data[property]);
        }
      }
    },error=>{
      this.errorsService.handle_error(error.status);
    }).catch(err=>this.working = false);
  }


  submit(e){
    if(this.group.valid){
      this.working = true;
      if(this.config.props.edit){
        let id = this.group.value.id;
        // when update
        var promiseObj = this.apiService.put(this.config.props.action+id,this.group.value);
     }else{
       // when insert
        var promiseObj = this.apiService.post(this.config.props.action,this.group.value);
     }
     promiseObj.then(result=>{
        this.working = false;
        this.afterSubmited.emit(result);
        this.toastr.success('Success', this.config.props.success_message);
      },error=>{
        this.working = false;
        this.toastr.error('Error', error.status+" error");
      }).catch(error=>{
        this.working = false;
        this.toastr.error('Error', JSON.stringify(error));
      });
    }
  }

  // build validators array for certain control
  buildValidatorsArray(rules):Array<ValidatorFn>{
    let validators:Array<ValidatorFn> = [];
    if(rules){
      rules.forEach(rule=>{
        validators.push(validations[rule]);
      });
    }
    return validators;
  }
}
