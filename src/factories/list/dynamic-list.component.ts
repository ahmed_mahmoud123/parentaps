import { Component,Input,Output,OnInit,EventEmitter} from '@angular/core';
import { APIService } from '../../providers/api.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector : "dynamic-list",
  templateUrl:"./dynamic-list.component.html",
  styleUrls:["./dynamic-list.component.css"]
})

/* Dynamic List Component to generate List depened on 'config' variable which has structure like {dataSource:'',colsNames:[],feildsName:[]} */
/* for 'config' variable structure kindly check models folder */
export class DynamicListComponent implements OnInit {

  data:Array<any>=[];
  total:number=0;
  page=1;
  last_page=0;
  working:boolean = true;
  @Output() rowClicked: any = new EventEmitter(); /*to fire event when row clicked and get row data*/
  @Input() config: any; /* {dataSource:"",colsNames:[],fieldsNames:[] }  */
  @Output() dataRecieved: any = new EventEmitter(); /* to fire event data come from the server */
  @Output() dynamicListObjectCreated: any = new EventEmitter(); /* this object will use for testing purpose */
  @Output() rowDeleted: any = new EventEmitter(); /*to fire event row deleted */

  constructor(private toastr: ToastrService,private apiService:APIService,private router:Router){

  }

  ngOnInit(){
    this.getData();
    this.dynamicListObjectCreated.emit(this);
  }

  // get data using dataSource to populate list
  getData(page=1){
    this.working = true;
    this.apiService.get(`${this.config.dataSource}${page}`).then(result=>{
      this.working = false;
      if(result.data){
        this.dataRecieved.emit(result);
        this.page = page;
        this.data = result.data;
        this.total=result.total;
        this.last_page=result.total_pages;
      }

    })
  }

  first(event){
    if(event)
      event.preventDefault();
    this.page=1;
    this.getData();
  }

  last(event){
    if(event)
      event.preventDefault();
    this.getData(this.last_page);
  }

  next(){
    if(event)
      event.preventDefault();
    this.getData(++this.page);
  }

  prev(){
    if(event)
      event.preventDefault();
    this.getData(--this.page);
  }


  clicked(e,row_id){
    e.preventDefault();
    this.rowClicked.emit(row_id);
  }

  // this component provide delete operation and view details
  btn_action(row,actionType){
    switch(actionType){
      case "view":
        this.view_row(row);
      break;
      case "delete":
        this.delete_row(row);
      break;
    }
  }

  view_row(row){
    this.router.navigate([this.config.detailsUrl+row.id]);
  }

  delete_row(row){
    if(confirm('are you sure ?')){
      this.working = true;
      this.apiService.delete(this.config.deleteUrl+row.id).then(result=>{
        this.working = false;
        this.rowDeleted.emit(true);
        this.toastr.success('Success', this.config.delete_message);
      },error=>{
        this.working = false;
        this.rowDeleted.emit(false);
        this.toastr.error('Error', error.status+" error");
      }).catch(error=>{
        this.working = false;
        this.rowDeleted.emit(false);
        this.toastr.error('Error', JSON.stringify(error));
      });
    }
  }
}
