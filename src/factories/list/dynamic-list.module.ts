import { NgModule } from '@angular/core';
import { DynamicListComponent } from './dynamic-list.component';
import { CommonModule } from '@angular/common';
import { APIService } from '../../providers/api.service';
import { RouterModule } from '@angular/router';
import { SpinnerModule } from 'angular2-spinner/dist';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';


@NgModule({
  imports: [
    CommonModule,
    SpinnerModule,
    RouterModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    })
  ],
  declarations: [
    DynamicListComponent
  ],
  exports: [
    DynamicListComponent
  ],
  entryComponents:[

  ],
  providers:[APIService]
})

export class DynamicListModule { }
