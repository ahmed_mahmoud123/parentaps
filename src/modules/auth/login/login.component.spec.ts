import { TestBed, async,inject } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { DynamicFormModule } from '../../../factories/form/dynamic-form.module'
import { By } from '@angular/platform-browser'
import { ErrorsModule } from '../../errors/errors.module';

import { RouterTestingModule } from '@angular/router/testing';
import{ RoutesHelper } from '../../../routes/routes';
import { DashboardComponent } from '../../../app/dashboard.component';
import { UserModule } from '../../users/users.module'

import { APIService } from '../../../providers/api.service';


describe('LoginComponent', () => {
  let fixture:any;
  let component:any;
  let service :APIService;
  let navigateSpy:any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent,
        DashboardComponent,
      ],
      providers: [
          APIService,

      ],
      imports:[ErrorsModule,UserModule,DynamicFormModule,RouterTestingModule.withRoutes(RoutesHelper.routes)]
    }).compileComponents();

    service = TestBed.get(APIService);

  }));

  beforeEach(async(()=>{
    this.fixture = TestBed.createComponent(LoginComponent);
    this.component = this.fixture.debugElement.componentInstance;
    this.navigateSpy = spyOn((<any>this.component).router, 'navigate');

  }));



  it('should create the Login Component', async(() => {
    expect(this.component).toBeTruthy();

  }));

  describe('Login()', () => {
    it('check email & password validatoin rules',()=>{
      this.fixture.detectChanges();
      this.fixture.whenStable().then(() => {
        let emailElement = this.fixture.debugElement.query(By.css('input[type="email"]')).nativeElement;
        let passwordElement = this.fixture.debugElement.query(By.css('input[type="password"]')).nativeElement;
        expect(emailElement.value).toBe('');
        expect(passwordElement.value).toBe('');
        emailElement.value = 'ahmedcs2012@gmail.com';
        emailElement.dispatchEvent(new Event('input'));
        expect(this.component.group.hasError("required",["email"])).toBeFalsy();
        expect(this.component.group.hasError("email",["email"])).toBeFalsy();
        expect(this.component.group.hasError("required",["password"])).toBeTruthy();
      });

    });

    it('check login function take "email" & "password" , return token and redirect to User List Page',(done)=>{
      this.component.ngOnInit();
      this.fixture.detectChanges();

      let emailElement = this.fixture.debugElement.query(By.css('input[type="email"]')).nativeElement;
      let passwordElement = this.fixture.debugElement.query(By.css('input[type="password"]')).nativeElement;
      emailElement.value = 'ahmedcs2012@gmail.com';
      passwordElement.value = 'ahmed';
      emailElement.dispatchEvent(new Event('input'));
      passwordElement.dispatchEvent(new Event('input'));
      this.component.dynamicFormObject.submit();
      this.component.dynamicFormObject.afterSubmited.subscribe(rs=>{
        expect(rs.token).toBeTruthy();
        expect(this.navigateSpy).toHaveBeenCalledWith(['dashboard/users']);
      });
      done();
    });

    it('redirect user , if he was already logged in ',()=>{
      //window.localStorage.setItem('token',"fakeToken");
      this.component.ngOnInit();
      this.fixture.detectChanges();
      if(window.localStorage.getItem('token'))
        expect(this.navigateSpy).toHaveBeenCalledWith(['dashboard/users']);
    });

  });

});
