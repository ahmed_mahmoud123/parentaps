import { Component,ElementRef } from '@angular/core';
import { LoginModel } from '../../../models/auth/login';
import { Router } from '@angular/router';

@Component({
  selector:"login",
  templateUrl:"./login.component.html",
  styleUrls:["./login.component.css"]
})

export class LoginComponent{
  // Model to build confi variable for building login form
  loginModel:LoginModel = new LoginModel();
  config:any; /* config for dynamic form component to build form group */
  group:any; /* holder for form group after created */
  dynamicFormObject:any; /* testing purpose */
  constructor(private router:Router,private elementRef: ElementRef){
    this.config = this.loginModel.config;
  }

  ngOnInit(){
    // check if user logged in before or not
    if(window.localStorage.getItem('token')){
      this.router.navigate(['dashboard/users']);
    }
  }

  ngAfterViewInit(){
    // change body backgroundColor
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#34495e';
  }

  afterSubmited(result){
    // to get result from server after the request sent
    if(result.token){
      window.localStorage.setItem('token',result.token);
      this.router.navigate(['dashboard/users']);
    }
  }

  /* testing purpose */
  onGroupCreated(data){
    this.group = data.group;
    this.dynamicFormObject=data.self;
  }
}
