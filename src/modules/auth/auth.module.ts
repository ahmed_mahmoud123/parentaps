import { NgModule } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { DynamicFormModule } from '../../factories/form/dynamic-form.module';
import { RouterModule } from '@angular/router';
@NgModule({
  declarations:[
    LoginComponent
  ],
  imports:[
    DynamicFormModule,
    RouterModule
  ]
})

export class AuthModule {}
