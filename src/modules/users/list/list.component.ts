import { Component } from '@angular/core';
import { ListUserModel } from '../../../models/users/list';
@Component({
  selector:"list-users",
  templateUrl:"./list.component.html",
  styleUrls:["./list.component.css"]
})

/* this component use Dynamic List Componet to generate list with pagination method , view row details method and delete certain row */
export class ListUsersComponent{
  config:any;
  /* listModel to build config variable for DynamicListComponent */
  listModel:ListUserModel = new ListUserModel();
  dynamicListObject:any;
  constructor(){}

  ngOnInit(){
    /* config = {dataSource:'',colsNames:["FirstName","Last Name",'avatar']} */
    this.config = this.listModel.config;
  }

  /* testing purpose */
  onDynamicListObjectCreated(dynamicListObject){
    this.dynamicListObject = dynamicListObject;
  }

}
