import { TestBed, async,inject } from '@angular/core/testing';
import { ListUsersComponent } from './list.component';

import { DynamicListModule } from '../../../factories/list/dynamic-list.module'

import { By } from '@angular/platform-browser'
import { ErrorsModule } from '../../errors/errors.module';
import { HttpModule } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import{ RoutesHelper } from '../../../routes/routes';
import { DashboardComponent } from '../../../app/dashboard.component';
import { APIService } from '../../../providers/api.service';

describe('ListUsersComponent', () => {
  let fixture:any;
  let component:any;
  let service :APIService;
  let navigateSpy:any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListUsersComponent,
        DashboardComponent,
      ],
      providers: [
          APIService
      ],
      imports:[ErrorsModule,HttpModule,DynamicListModule,RouterTestingModule.withRoutes([])]
    }).compileComponents();

    service = TestBed.get(APIService);
  }));

  beforeEach(async(()=>{
    this.fixture = TestBed.createComponent(ListUsersComponent);
    this.component = this.fixture.debugElement.componentInstance;
    this.component.ngOnInit();
    this.fixture.detectChanges();
    this.navigateSpy = spyOn((<any>this.component.dynamicListObject).router, 'navigate');

  }));



  it('should create the ListUsersComponent ', async(() => {
    expect(this.component).toBeTruthy();
  }));

  it('Check Get Users data function()', async(() => {
    this.component.ngOnInit();
    this.fixture.detectChanges();
    this.component.dynamicListObject.getData();
    this.component.dynamicListObject.dataRecieved.subscribe(result=>{
      expect(result.data.length).toBeGreaterThan(0);
      expect(result.data[0].first_name).toBeTruthy();
    });
  }));

  it('Check pagination "next" function()', async(() => {
    this.component.ngOnInit();
    this.fixture.detectChanges();
    this.component.dynamicListObject.next();
    this.component.dynamicListObject.dataRecieved.subscribe(result=>{
      expect(result.data.length).toBeGreaterThanOrEqual(0);
      expect(result.data[0].first_name).toBeTruthy();
    });
  }));

  it('Check pagination "Previous" function()', async(() => {
    this.component.ngOnInit();
    this.fixture.detectChanges();
    this.component.dynamicListObject.page = 4;
    this.component.dynamicListObject.prev();
    this.component.dynamicListObject.dataRecieved.subscribe(result=>{
      expect(result.data.length).toBeGreaterThanOrEqual(0);
      expect(result.data[0].first_name).toBeTruthy();
    });
  }));

  describe("List Action Delete & View ",()=>{
    it("Check delete certain row from list",()=>{
      this.component.ngOnInit();
      this.fixture.detectChanges();
      this.component.dynamicListObject.delete_row({id:1})
      this.component.dynamicListObject.rowDeleted.subscribe(result=>{
        expect(result).toBeTruthy();
      });
    });

    it("Check view certain row details list",()=>{
      this.component.ngOnInit();
      this.fixture.detectChanges();
      this.component.dynamicListObject.view_row({id:1})
      expect(this.navigateSpy).toHaveBeenCalledWith(['/dashboard/users/view/1']);
    })
  });

});
