import { TestBed, async,inject } from '@angular/core/testing';
import { UserProfileComponent } from './profile.component';

import { DynamicFormModule } from '../../../factories/form/dynamic-form.module'
import { DynamicListModule } from '../../../factories/list/dynamic-list.module'

import { By } from '@angular/platform-browser'
import { ErrorsModule } from '../../errors/errors.module';

import { RouterTestingModule } from '@angular/router/testing';
import{ RoutesHelper } from '../../../routes/routes';
import { DashboardComponent } from '../../../app/dashboard.component';
import { APIService } from '../../../providers/api.service';

describe('ProfileComponent', () => {
  let fixture:any;
  let component:any;
  let service :APIService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        UserProfileComponent,
        DashboardComponent,
      ],
      providers: [
          APIService
      ],
      imports:[ErrorsModule,DynamicFormModule,RouterTestingModule.withRoutes([])]
    }).compileComponents();

    service = TestBed.get(APIService);
  }));

  beforeEach(async(()=>{
    this.fixture = TestBed.createComponent(UserProfileComponent);
    this.component = this.fixture.debugElement.componentInstance;

  }));



  it('should create the UserProfile Component', async(() => {
    expect(this.component).toBeTruthy();

  }));

  describe('Insert New User function()', () => {
    it('check Job & Name validatoin rules',()=>{
      this.component.generateConfig(undefined,{add:true});
      this.fixture.detectChanges();
      this.fixture.whenStable().then(() => {
        let nameElement = this.fixture.debugElement.query(By.css('input[name="name"]')).nativeElement;
        let jobElement = this.fixture.debugElement.query(By.css('input[name="job"]')).nativeElement;
        expect(nameElement.value).toBe('');
        expect(jobElement.value).toBe('');
        nameElement.value = 'ahmedcs2012@gmail.com';
        nameElement.dispatchEvent(new Event('input'));
        expect(this.component.group.hasError("required",["name"])).toBeFalsy();
        expect(this.component.group.hasError("required",["job"])).toBeTruthy();
      });

    });

    it('check insert new user ',(done)=>{
      this.component.generateConfig(undefined,{add:true});
      this.fixture.detectChanges();
      this.fixture.whenStable().then(() => {
        let nameElement = this.fixture.debugElement.query(By.css('input[name="name"]')).nativeElement;
        let jobElement = this.fixture.debugElement.query(By.css('input[name="job"]')).nativeElement;
        nameElement.value = 'ahmed';
        jobElement.value = 'Developer';
        nameElement.dispatchEvent(new Event('input'));
        jobElement.dispatchEvent(new Event('input'));
        this.component.dynamicFormObject.submit();
        this.component.dynamicFormObject.afterSubmited.subscribe(rs=>{
          expect(rs.createdAt).toBeTruthy();
        },error=>{
          expect(true).toBe(false);
        });

      });
      done();
    });
  });


  describe('Update User function()', () => {
    it('check ID & Job & Name validatoin rules',()=>{
      this.component.generateConfig(undefined,{edit:true});
      this.fixture.detectChanges();
      this.fixture.whenStable().then(() => {
        let idElement = this.fixture.debugElement.query(By.css('input[name="id"]')).nativeElement;
        let nameElement = this.fixture.debugElement.query(By.css('input[name="name"]')).nativeElement;
        let jobElement = this.fixture.debugElement.query(By.css('input[name="job"]')).nativeElement;
        expect(idElement.value).toBe('');
        expect(nameElement.value).toBe('');
        expect(jobElement.value).toBe('');
        nameElement.value = 'ahmedcs2012@gmail.com';
        nameElement.dispatchEvent(new Event('input'));
        expect(this.component.group.hasError("required",["name"])).toBeFalsy();
        expect(this.component.group.hasError("required",["job"])).toBeTruthy();
        expect(this.component.group.hasError("required",["id"])).toBeTruthy();
      });

    });

    it('check update user ',(done)=>{
      this.component.generateConfig(undefined,{edit:true});
      this.fixture.detectChanges();
      this.fixture.whenStable().then(() => {
        let idElement = this.fixture.debugElement.query(By.css('input[name="id"]')).nativeElement;
        let nameElement = this.fixture.debugElement.query(By.css('input[name="name"]')).nativeElement;
        let jobElement = this.fixture.debugElement.query(By.css('input[name="job"]')).nativeElement;
        idElement.value = '1';
        nameElement.value = 'ahmed';
        jobElement.value = 'Developer';
        nameElement.dispatchEvent(new Event('input'));
        jobElement.dispatchEvent(new Event('input'));
        idElement.dispatchEvent(new Event('input'));
        this.component.dynamicFormObject.submit();
        this.component.dynamicFormObject.afterSubmited.subscribe(rs=>{
          expect(rs.updatedAt).toBeTruthy();
        },error=>{
          expect(true).toBe(false);
        });

      });
      done();
    });
  });

  describe('View User Details function()', () => {
    it('Check View User Component',()=>{
      this.component.generateConfig(1,undefined);
      this.fixture.detectChanges();
      this.fixture.whenStable().then(() => {
        expect(this.component.group.controls['avatar'].value).toBe('');
        expect(this.component.group.controls['first_name'].value).toBe('');
        expect(this.component.group.controls['last_name'].value).toBe('');
      });
    });

    it('Check 404 page when user enter fake ID',()=>{});

    it('get user Details function()',(done)=>{
      service.get("api/users/1").then((user:any)=>{
        expect(user.data.id).toBeTruthy();
        done();
      }).catch(err=>{
        expect(true).toBe(false);
      });
    });
  });

});
