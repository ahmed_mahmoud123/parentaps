import { Component } from '@angular/core';
import { UserProfileModel } from '../../../models/users/profile';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector:"login",
  templateUrl:"./profile.component.html",
  styleUrls:["./profile.component.css"]
})

export class UserProfileComponent{
  // Model to build confi variable for building Profile forms
  /* I used this componet for inserting , updating and view user details */
  profileModel:UserProfileModel = new UserProfileModel();
  config:any; /* config for dynamic form component to build form group */
  group:any;/* holder for form group after created */
  dynamicFormObject:any;/* testing purpose */
  constructor(private activatedRoute:ActivatedRoute){}

  ngOnInit(){
    // get the user id from URL to determine the request if it is view or update or insert
    let user_id = this.activatedRoute.snapshot.params["id"];
    // get the operation from routes if add or edit
    let route_data= this.activatedRoute.data['value'];
    this.generateConfig(user_id,route_data);
  }

  generateConfig(user_id,route_data){
    // if user id found then this operation is view details operation
    if(user_id){
      this.config = this.profileModel.generateViewConfig(user_id);
    }else if( route_data.add ){ /* if the routes come with value 'add' then generate add config for form building */
      this.config = this.profileModel.generateAddConfig();
    }else if(route_data.edit){ /* if the routes come with value 'edit' then generate edit config for form building */
      this.config = this.profileModel.generateEditConfig();
    }
  }

  /* testing purpose */
  onGroupCreated(data){
    this.group = data.group;
    this.dynamicFormObject=data.self;
  }
}
