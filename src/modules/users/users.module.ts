import { NgModule } from '@angular/core';

import { ListUsersComponent } from './list/list.component';
import { UserProfileComponent } from './profile/profile.component';
import { DynamicFormModule } from '../../factories/form/dynamic-form.module';
import { RouterModule } from '@angular/router';
import { DynamicListModule } from '../../factories/list/dynamic-list.module';
@NgModule({
  declarations:[
    ListUsersComponent,
    UserProfileComponent,

  ],
  imports:[
    RouterModule,
    DynamicListModule,
    DynamicFormModule
  ]
})

export class UserModule {}
