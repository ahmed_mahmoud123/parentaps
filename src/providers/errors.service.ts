import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class ErrorsService {


  constructor(public router: Router) {

  }

  handle_error(err_status){
    switch(err_status){
      case 404:
        this.router.navigate(['/dashboard/404']);
      break;
    }
  }

}
