import { Injectable } from '@angular/core';
import { Http,Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class APIService {
  headers:Headers;
  api_url:string;

  constructor(public http: Http) {
    this.api_url="https://reqres.in/";
    this.headers = new Headers ({ 'Content-Type': 'application/json' });
  }

  prepareRequestHeader(){
    this.headers = new Headers ({'Content-Type': 'application/json' });
    return {headers:this.headers};
  }

  post(urn,object){
    return this.http.post(`${this.api_url}${urn}`,JSON.stringify(object),this.prepareRequestHeader()).map(resp=>resp.json()).toPromise();
  }

  put(urn,object){
    return this.http.put(`${this.api_url}${urn}`,JSON.stringify(object),this.prepareRequestHeader()).map(resp=>resp.json()).toPromise();
  }

  get(urn){
    return this.http.get(`${this.api_url}${urn}`,this.prepareRequestHeader()).map(resp=>resp.json()).toPromise();
  }

  delete(urn){
    return this.http.delete(`${this.api_url}${urn}`,this.prepareRequestHeader()).map(resp=>resp.json()).toPromise();
  }



}
