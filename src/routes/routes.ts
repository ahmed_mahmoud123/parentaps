import {  Routes } from '@angular/router';
import { LoginComponent } from '../modules/auth/login/login.component';
import { ListUsersComponent } from '../modules/users/list/list.component';
import { UserProfileComponent } from '../modules/users/profile/profile.component';
import { Error44Component } from '../modules/errors/404/404.component';

import { DashboardComponent } from '../app/dashboard.component';


export class RoutesHelper{

  static routes:Routes=[
    {
      path: "login",
      component: LoginComponent,
      data: { title: 'Login Page'}
    },
    // nested routing to provide layout for child components 
    {
      path: "dashboard",
      component: DashboardComponent,
      children:[
        {path: '', redirectTo: 'users',pathMatch: 'full'},
        {path: 'users', component: ListUsersComponent,data: { title: 'List Users'}},
        {path: 'users/add', component: UserProfileComponent,data: { title: 'Add User',add:true}},
        {path: 'users/edit', component: UserProfileComponent,data: { title: 'Edit User',edit:true}},
        {path: 'users/view/:id', component: UserProfileComponent,data: { title: 'User Details'}},
        {path: '404', component: Error44Component,data: { title: 'Page Not Found'}},
      ],

    }
    ,
    {
      path: '',
      redirectTo: '/login',
      pathMatch: 'full'
    },
    { path: '**', component: LoginComponent }
  ]
}
