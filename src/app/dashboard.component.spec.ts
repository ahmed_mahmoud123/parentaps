import { TestBed, async,inject } from '@angular/core/testing';
import { DashboardComponent } from './dashboard.component';



import { RouterTestingModule } from '@angular/router/testing';
import{ RoutesHelper } from '../routes/routes';
import { APIService } from '../providers/api.service';
import { HttpModule,Http } from '@angular/http';
import { Router } from '@angular/router';
import { DynamicFormModule } from '../factories/form/dynamic-form.module'

import { LoginComponent } from '../modules/auth/login/login.component';
describe('DashboardComponent', () => {
  let fixture:any;
  let component:any;
  let service :APIService;
  let navigateSpy:any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent,
        LoginComponent
      ],
      providers: [
          APIService,
      ],
      imports:[DynamicFormModule,RouterTestingModule.withRoutes([
        {
          path: "login",
          component: LoginComponent,
          data: { title: 'Login Page'}
        }
      ])]
    }).compileComponents();

    service = TestBed.get(APIService);
  }));

  beforeEach(async(()=>{
    this.fixture = TestBed.createComponent(DashboardComponent);
    this.component = this.fixture.debugElement.componentInstance;
    this.navigateSpy = spyOn((<any>this.component).router, 'navigate');
  }));



  it('should create the DashboardComponent Component', async(() => {
    expect(this.component).toBeTruthy();
  }));

  it('should not let users pass when not logged in', async(() => {
    window.localStorage.setItem('token',"fakeToken");
    this.component.ngOnInit();
    this.fixture.detectChanges();
    if(!window.localStorage.getItem('token'))
      expect(this.navigateSpy).toHaveBeenCalledWith(['login']);
  }));


});
