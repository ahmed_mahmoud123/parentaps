import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router , ActivatedRoute ,NavigationEnd } from '@angular/router';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  current_title:string;
  constructor(private router:Router,private activatedRoute:ActivatedRoute,private title:Title){

  }

  ngOnInit(){
    // function to get page title from routes like set page title "List Users"
    this.router.events
      .filter((event) => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map((route) => {
        while (route.firstChild) route = route.firstChild;
        return route;
      })
      .filter((route) => route.outlet === 'primary')
      .mergeMap((route) => route.data)
      .subscribe((event) => {
        this.current_title = event['title'];
        this.title.setTitle(event['title']);
      });
  }
}
