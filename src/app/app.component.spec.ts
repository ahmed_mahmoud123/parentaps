import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RouterTestingModule } from '@angular/router/testing';

import{ RoutesHelper } from '../routes/routes';

import { NavigationEnd } from '@angular/router';
import { AuthModule } from '../modules/auth/auth.module';
import { UserModule } from '../modules/users/users.module';
import { ErrorsModule } from '../modules/errors/errors.module';
import { By } from '@angular/platform-browser'

import { DashboardComponent } from './dashboard.component';
describe('AppComponent', () => {
  let fixture:any;
  let component:any;
  let navigateSpy:any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        DashboardComponent
      ],
      imports:[ErrorsModule,UserModule,AuthModule,RouterTestingModule.withRoutes(RoutesHelper.routes)]
    }).compileComponents();
  }));

  beforeEach(async(()=>{
    this.fixture = TestBed.createComponent(AppComponent);
    this.component = this.fixture.debugElement.componentInstance;
    this.navigateSpy = spyOn((<any>this.component).router, 'navigate');

  }));

  it('should create the app', async(() => {
    expect(this.component).toBeTruthy();
  }));


  

});
