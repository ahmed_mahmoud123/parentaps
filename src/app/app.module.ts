import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard.component';

import {  RouterModule } from '@angular/router';
import { AuthModule } from '../modules/auth/auth.module';
import { ErrorsModule } from '../modules/errors/errors.module';

import { UserModule } from '../modules/users/users.module';

import { RoutesHelper } from '../routes/routes';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent
  ],
  imports: [
    ErrorsModule,
    BrowserModule,
    AuthModule,
    UserModule,
    RouterModule.forRoot(RoutesHelper.routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
