import { Component,ElementRef } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls:['./dashboard.component.css']
})


/* this component to provide deep routing to can set layout for pages like header and footer and apply common functions for alot of components*/
export class DashboardComponent {
  constructor(private router:Router,private elementRef: ElementRef){

  }

  ngOnInit(){
    if(!window.localStorage.getItem('token')){
      this.router.navigate(['login']);
    }
  }

  ngAfterViewInit(){
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#FFF';
  }

  logout(evt){
    evt.preventDefault();
    window.localStorage.clear();
    this.router.navigate(['login']);
  }
}
